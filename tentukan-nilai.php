<?php
function tentukan_nilai($number)
{
    if ($number >= 85) {
        echo "$number = Sangat Baik<br>";
    }else if($number >= 70) {
        echo "$number = Baik<br>";
    }else if($number >= 60) {
        echo "$number = Cukup<br>";
    }else if($number >= 0) {
        echo "$number = Kurang<br>";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>